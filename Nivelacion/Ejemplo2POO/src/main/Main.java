package main;

import clases.Operacion;

import java.util.Scanner;
import clases.Suma;
import clases.Resta;
import clases.Multiplicar;
import clases.Dividir;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Suma suma = new Suma();
        Resta resta = new Resta();
        Multiplicar multiplicar = new Multiplicar();
        Dividir dividir = new Dividir();

        System.out.println("Ingrese un numero: ");
        double numero1=scanner.nextDouble();
        suma.setNumero1(numero1);
        resta.setNumero1(numero1);
        multiplicar.setNumero1(numero1);
        dividir.setNumero1(numero1);
        System.out.println("Ingrese otro número: ");
        double numero2=scanner.nextDouble();
        suma.setNumero2(numero2);
        resta.setNumero2(numero2);
        multiplicar.setNumero2(numero2);
        dividir.setNumero2(numero2);

        System.out.println("El resultado de la suma es:"+suma.sumar());
        System.out.println("El resultado de la resta es:"+resta.restar());
        System.out.println("El resultado de la multiplicación es:"+multiplicar.multiplicar());
        System.out.println("El resultado de la división es:"+dividir.dividir());

    }
}
