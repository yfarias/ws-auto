package main;

import clases.BarcoPirata;

public class Main {

    public static void main(String[] args) {
        BarcoPirata fragata = new BarcoPirata();

        fragata.setX(450);
        fragata.setY(180);

        fragata.moverPosicion(50,20);
        fragata.conocerPosicion();
        fragata.disparar();
    }

}
