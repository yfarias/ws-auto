package main;

import java.util.Scanner;

public class Main {
    public static void main(String [] args){
        //Instancia a una clase
        Scanner teclado = new Scanner(System.in);

        //Definción e inicilización de variables
        System.out.print("Ingrese el largo: ");
        double largo=teclado.nextDouble();
        System.out.print("Ingrese el ancho: ");
        double ancho=teclado.nextDouble();
        System.out.print("Ingrese el alto: ");
        double alto=teclado.nextDouble();
        double volumen;

        //Calculo del volumen acorde al requerimiento solicitado
        volumen = largo*ancho*alto;

        //Imprimir el resultado del calculo anterior
        System.out.println("El volumen de la caja es: "+volumen);
        //System.out.println("Otro mensaje");
        //System.out.println("Mensaje adicional");
    }
}
