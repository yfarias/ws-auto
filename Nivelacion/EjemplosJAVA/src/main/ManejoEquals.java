package main;

public class ManejoEquals {
    public static void main(String[] args) {
        String nombre1="Saludo";
        String nombre2="Saludo";

        System.out.println("Comprobación 1: "+ nombre1==nombre2);
        System.out.println("Comprobación 2: "+ nombre1.equals(nombre2));

        System.out.println(nombre1.charAt(0));//imprime posición
        System.out.println(nombre1.length());//longitud
        System.out.println(nombre1.toUpperCase());//MAYUSCULAS
        System.out.println(nombre1.toLowerCase());//minisculas

    }
}
