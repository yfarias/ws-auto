package condicionales;

import java.util.Scanner;

public class CondicionalIf {
    public static void main(String[] args) {
        //Escribir un codigo donde se valide que al momento de que el usuario
        //ingrese la edad de una persona, validesmos si es menor o mayor de edad
        System.out.print("Por favor ingrese su edad: ");
        Scanner teclado = new Scanner(System.in);
        byte edad = teclado.nextByte();

        if (edad >= 18) {
            System.out.println("Es mayor de edad");
        } else {
            System.out.println("Es menor de edad");
        }

    }
}
