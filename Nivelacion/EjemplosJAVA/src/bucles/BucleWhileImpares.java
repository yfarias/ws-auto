package bucles;

import java.util.Scanner;

public class BucleWhileImpares {
    //Desarrollar un programa que imprima los números impares entre 1 y 25.
    public static void main(String[] args) {
        System.out.println("Ingrese el número: ");
        Scanner teclado = new Scanner(System.in);
        int inicio = 0;
        int numero = teclado.nextInt();

        while (inicio <= numero) {
            if ((inicio % 2) != 0) {
                System.out.println("El número: " + inicio + " es impar");
            } else {
                System.out.println("El número: " + inicio + " es par");
            }
            inicio++;
        }
    }
}
