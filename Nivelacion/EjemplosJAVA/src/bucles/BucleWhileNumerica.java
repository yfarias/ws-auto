package bucles;

public class BucleWhileNumerica {
    public static void main(String[] args) {
        int x = 0;

        while (x <= 3) {
            System.out.println("Ingreso al ciclo While: " + x + " veces");
            x++;
        }
    }
}
