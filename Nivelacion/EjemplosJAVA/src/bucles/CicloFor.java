package bucles;

public class CicloFor {
    public static void main(String[] args) {
        for (int i = 0; i <= 10; i++) {
            System.out.println("Número: " + i);
        }

        //Desarrollar un programa que muestre los números pares entre 0 – 20
        // de forma ascendente.
        for (int i = 0; i <= 20; i += 2) {
            System.out.println("Número par: " + i);
        }

        //Desarrollar un programa que muestre los números pares entre 0 – 20
        // de forma descendente.
        for (int i = 20; i >= 0; i -= 2) {
            System.out.println("Número par descendente: " + i);
        }
    }
}
