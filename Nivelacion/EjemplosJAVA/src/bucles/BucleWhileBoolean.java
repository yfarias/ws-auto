package bucles;

public class BucleWhileBoolean {
    public static void main(String[] args) {
        boolean x = true;

        while(x){
            System.out.println("X es verdadero");
            x=false;
        }
    }
}
