package clases;

public class EmpleadoSalarioMinimo extends Empleado {

    public EmpleadoSalarioMinimo(String nombre, double cantidadHoras, double valorHora) {
        super(nombre, cantidadHoras, valorHora);
    }

    @Override
    public double calcularSalario() {
        return (this.getCantidadHoras()*this.getValorHora());
    }
}
