package main;

import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import clases.EmpleadoAdmin;
import clases.EmpleadoGerente;
import clases.EmpleadoSalarioMinimo;

public class Main {
    public static void main(String[] args) {

        EmpleadoSalarioMinimo empleadoSalarioMinimo = new EmpleadoSalarioMinimo("Pedro", 48, 20000);
        EmpleadoAdmin empleadoAdmin = new EmpleadoAdmin("Adriana", 96, 30000);
        empleadoAdmin.setPorcentajeSalud(4);
        EmpleadoGerente empleadoGerente = new EmpleadoGerente("Andres", 120, 50000);
        empleadoGerente.setPorcentajeSalud(5);
        empleadoGerente.setPorcentajePension(5);


        System.out.println("El salario a pagar al empleado Min es: " + empleadoSalarioMinimo.calcularSalario());
        System.out.println("El salario a pagar al empleado Admin es: " + empleadoAdmin.calcularSalario());
        System.out.println("El salario a pagar al empleado Gerente es: " + empleadoGerente.calcularSalario());


    }
}
