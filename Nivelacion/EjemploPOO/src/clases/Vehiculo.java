package clases;

public class Vehiculo {
    //Caracteristicas:Atributos
    private String marca;
    private String modelo;
    private String color;
    private String tipo;
    private String password;
    private int puertas;

    //Transporte de datos -- Getter y Setter(Metodos)
    //set-- asignar o establcer
    //get-- obtener
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }


}
