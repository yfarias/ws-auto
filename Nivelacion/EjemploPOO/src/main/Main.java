package main;

import clases.Vehiculo;

public class Main {
    public static void main(String[] args) {
        //Instancia de una clase
        Vehiculo vehiculo=new Vehiculo();

        //Asiganrle valor atributo marca
        vehiculo.setMarca("Marza");

        //Consultado el valor que tiene la marca
        System.out.println("La marca del vehiculo es:"+vehiculo.getMarca());

        vehiculo.setPuertas(4);

        System.out.println("La cantidad de puertas del vehiculo es:"+vehiculo.getPuertas());
    }
}
