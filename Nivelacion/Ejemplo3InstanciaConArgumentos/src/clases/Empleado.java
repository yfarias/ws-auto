package clases;

public class Empleado {
    private double valorHora;
    private double cantidadHoras;

    public Empleado(double vhora,double choras){//parametros
        this.valorHora=vhora;
        this.cantidadHoras=choras;
    }

    protected double getValorHora(){
        return  this.valorHora;
    }

    protected double getCantidadHoras(){
        return this.cantidadHoras;
    }
}
