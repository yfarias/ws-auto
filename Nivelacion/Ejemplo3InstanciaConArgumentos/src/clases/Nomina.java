package clases;

public class Nomina extends Empleado {

    public Nomina(double vhora, double choras) {
        super(vhora, choras);
    }

    public double calcularSalario() {
        return (this.getCantidadHoras() * this.getValorHora());
    }
}
