package clases;

public class Nomina extends Empleado{
    public Nomina(double vhora, double choras) {
        super(vhora, choras);
    }

    public double calcularSalario() {
        return (getCantidadHoras() * getValorHora());
    }

    public double calcularSalario(double impuesto){
        return (calcularSalario()-(calcularSalario()*(impuesto/100)));
    }
}
