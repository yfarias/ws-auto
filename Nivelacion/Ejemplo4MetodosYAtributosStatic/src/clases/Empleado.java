package clases;

public class Empleado {
    private double valorHora;
    private double cantidadHoras;

    protected Empleado(double valorHoras, double cantidadHora) {//Parámetros
        valorHora = valorHoras;
        cantidadHoras = cantidadHora;
    }

    protected double getValorHora() {
        return valorHora;
    }

    protected double getCantidadHoras(){
        return cantidadHoras;
    }
}
