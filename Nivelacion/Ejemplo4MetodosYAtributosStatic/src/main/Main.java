package main;

import clases.Nomina;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;


public class Main {
    public static void main(String[] args) {
        Logger logger=Logger.getLogger(Main.class.getName());
try {
    double cantidadHoras = Double.parseDouble(JOptionPane.showInputDialog("Introduzca las horas trabajadas:"));
    double valorHora = Double.parseDouble(JOptionPane.showInputDialog("Ingrese el valor de la hora:"));

    Nomina nomina = new Nomina(valorHora, cantidadHoras);

    logger.log(Level.INFO, "El calculo del salario a pagar es: {0}", nomina.calcularSalario());
    logger.log(Level.INFO, "El calculo del salario a pagar con 10% de descuento es: {0}", nomina.calcularSalario(10));
}catch (NumberFormatException numberFormatException){
    JOptionPane.showMessageDialog(null,"El valor ingresado no es correcto");
    logger.log(Level.WARNING, "El valor ingresado no es correcto");
}catch (Exception exception){
    JOptionPane.showMessageDialog(null,"No es posible procesar su solicitud");
    logger.log(Level.WARNING, "No es posible procesar su solicitud");
}
    }
}
