package automationtesting.aperturapagina;

import automationtesting.Constantes;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.esperar;
import static org.junit.Assert.assertEquals;


public class TestAbrirPagina {

    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void abrirpagina() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_AUTOMATION_TESTING);
        esperar(TIEMPO_ESPERA_CORTO);
    }


    @After
    public void finish() {
        driver.quit();
    }
}
