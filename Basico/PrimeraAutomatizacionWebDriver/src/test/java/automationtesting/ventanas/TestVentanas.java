package automationtesting.ventanas;

import automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static automationtesting.Constantes.*;
import static org.junit.Assert.assertEquals;

public class TestVentanas {

    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void manejoVentanas() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_VENTANAS);

        String ventanaInicial = driver.getWindowHandle();

        System.out.println("ID Venta: " + ventanaInicial);
        System.out.println("Titulo Venta 1: " + driver.getTitle());

        driver.findElement(By.xpath(BUTTON_CLICK)).click();

        for(String manejadorVentana : driver.getWindowHandles()){
            if(!ventanaInicial.contentEquals(manejadorVentana)){
                driver.switchTo().window(manejadorVentana);
                break;
            }
        }

        System.out.println("Titulo Venta 2: " + driver.getTitle());
        driver.close();
        driver.switchTo().window(ventanaInicial);
        System.out.println("Titulo Venta 1: " + driver.getTitle());
        assertEquals("If you set the target attribute to \"_blank\" , the link will open in a new browser window or a new tab", driver.findElement(By.xpath("//*[@id='Tabbed']/p")).getText());
    }

    @Test
    public void manejo2Ventanas() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_VENTANAS);

        String ventanaInicial = driver.getWindowHandle();

        System.out.println("ID Venta: " + ventanaInicial);
        System.out.println("Titulo Venta 1: " + driver.getTitle());

        driver.findElement(By.xpath("//a[@href='#Seperate']")).click();
        driver.findElement(By.xpath("//*[@id='Seperate']/button")).click();

        for(String manejadorVentana : driver.getWindowHandles()){
            if(!ventanaInicial.contentEquals(manejadorVentana)){
                driver.switchTo().window(manejadorVentana);
                break;
            }
        }

        System.out.println("Titulo Venta 2: " + driver.getTitle());
        driver.close();
        driver.switchTo().window(ventanaInicial);
        System.out.println("Titulo Venta 1: " + driver.getTitle());
        assertEquals("click the button to open a new window with some specifications", driver.findElement(By.xpath("//*[@id='Seperate']/p")).getText());
    }

    @Test
    public void manejo3Ventanas() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_VENTANAS);

        driver.findElement(By.xpath("//a[@href='#Multiple']")).click();
        driver.findElement(By.xpath("//*[@id='Multiple']/button")).click();

        Utilidades.esperar(TIEMPO_ESPERA_CORTO);

        int i=1;
        for(String manejadorVentana : driver.getWindowHandles()){
            driver.switchTo().window(manejadorVentana);
            System.out.println("Titulo Venta "+i+" es: " + driver.getTitle());
            i++;
        }
    }

    @After
    public void finish() {
        driver.quit();
    }
}
