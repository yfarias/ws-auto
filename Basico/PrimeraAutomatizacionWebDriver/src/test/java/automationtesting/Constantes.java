package automationtesting;

public class Constantes {

    public static final String URL_PRACTICE_AUTOMATION_TESTING="http://practice.automationtesting.in/";
    public static final String URL_PRACTICE_DEMO_AUTOMATION_TESTING_FRAME="http://demo.automationtesting.in/Frames.html";
    public static final String URL_PRACTICE_DEMO_AUTOMATION_TESTING_REGISTRO="http://demo.automationtesting.in/Register.html";
    public static final String URL_PRACTICE_DEMO_AUTOMATION_TESTING_ALERTAS="http://demo.automationtesting.in/Alerts.html";
    public static final String URL_PRACTICE_DEMO_AUTOMATION_TESTING_LOGIN="http://practice.automationtesting.in/my-account/";
    public static final String URL_PRACTICE_DEMO_AUTOMATION_TESTING_VENTANAS="http://demo.automationtesting.in/Windows.html";
    public static final String URL_PRACTICE_TABLAS="http://demo.guru99.com/test/web-table-element.php#";
    public static final String LINK_MY_ACCOUNT="My Account";
    public static final String USER_NAME="username";
    public static final String CAMPO_PASSWORD="password";
    public static final String BTN_LOGIN="login";
    public static final String USERNAME="pruebaregistro128@correo.com";
    public static final String PASSWORD="Colombia1234+.,--";
    public static final String PASSWORD_ERROR="Colombia1234";
    public static final String TEXTO_VALIDACION="Hello pruebaregistro128 (not pruebaregistro128? Sign out)";
    public static final String TEXTO_VALIDACION_ERROR="ERROR: The password you entered for the username pruebaregistro128@correo.com is incorrect. Lost your password?";
    public static final long TIEMPO_ESPERA_MINIMO=1;
    public static final long TIEMPO_ESPERA_CORTO=2;
    public static final long TIEMPO_ESPERA_NORMAL=5;
    public static final long TIEMPO_ESPERA_LARGO=10;
    public static final String LABEL_BIENVENIDA="//*[@id='page-36']/div/div[1]/div/p[1]";
    public static final String LABEL_AUTENTICACION_FALLIDA="//*[@id='page-36']/div/div[1]/ul/li";
    public static final String TEXTBOX_IFRAME="/html/body/section/div/div/div/input";
    public static final String PRIMER_FRAME="singleframe";
    public static final String TITULO_PAGINA_AUTOMATION="Automation Demo Site";
    public static final String LABEL_AUTOMATION="//*[@id='header']/div/div/div/div[2]/h1";
    public static final String FEMENIMO="FeMale";
    public static final String MASCULINO="Male";
    public static final String CONTENEDOR_GENERO="//div[@class='col-md-4 col-xs-4 col-sm-4']/label";
    public static final String CONTENEDOR_SKILLS="//select[@id='Skills']/option";
    public static final String CONTENEDOR_PAIS="//select[@id='country']/option";
    public static final String CONTENEDOR_LENGUAJE="//ul[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all']/li/a";
    public static final String LOCATOR_PASSWORD="//input[@id='firstpassword']";
    public static final String BUTTON_CLICK="//*[@id='Tabbed']//button";
    public static final String LOCATOR_LENGUAJE="msdd";
    public static final String CRICKET="Cricket";
    public static final String MOVIES="Movies";
    public static final String HOCKEY="Hockey";
    public static final String SKILL_C="C";
    public static final String PAIS_JAPON="Japan";



}
