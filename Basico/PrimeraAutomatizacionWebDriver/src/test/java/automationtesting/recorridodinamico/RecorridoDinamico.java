package automationtesting.recorridodinamico;

import automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Builder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.*;
import static automationtesting.Utilidades.mapeoXpath;

public class RecorridoDinamico {

    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void recorridoDinamicoRadios() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_REGISTRO);
        //Seleccionar Genero
        seleccionarDeUnaLista(mapeoXpath(CONTENEDOR_GENERO,driver), FEMENIMO);
        //Seleccionar Skill
        seleccionarDeUnaLista(mapeoXpath(CONTENEDOR_SKILLS,driver), SKILL_C);
        //Seleccionar Pais
        seleccionarDeUnaLista(mapeoXpath(CONTENEDOR_PAIS,driver), PAIS_JAPON);
        esperar(5);
    }

    @Test
    public void recorridoDinamicoCheck() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_REGISTRO);

        List<WebElement> hobbies = driver.findElements(By.xpath("//div[@class='col-md-4 col-xs-4 col-sm-4']/div/label"));
        String opciones = "Cricket,Hockey,Movies";

        List<String> listaopciones = new ArrayList<>();
        listaopciones.addAll(Arrays.asList(opciones.split(",")));

        for(int i=0;i<listaopciones.size();i++){
            seleccionarDeUnCheckBox(hobbies, listaopciones.get(i), driver);
        }

        esperar(2);

    }

    @Test
    public void Test() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_REGISTRO);
        //Seleccionar Genero
        List<WebElement> contenedor= driver.findElements(By.xpath(" //ul[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all']/li/a"));
        seleccionarDeUnaLista(contenedor, "Arabic");
        esperar(5);
    }


    @After
    public void finish() {
        driver.quit();
    }
}
