package automationtesting.tablas;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.esperar;

public class TestTablas {
    private static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void validarCantidadFilasColumnas() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_TABLAS);

        List columnas = driver.findElements(By.xpath("//table/thead/tr/th"));
        System.out.println("Cantidad de columnas:" + columnas.size());

        List filas = driver.findElements(By.xpath("//table/tbody/tr"));
        System.out.println("Cantidad de filas:" + filas.size());

    }

    @Test
    public void consultarValorTabla() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_TABLAS);

        WebElement tabla = driver.findElement(By.xpath("//table[@class='dataTable']"));

        WebElement fila = tabla.findElement(By.xpath("//tbody/tr[2]"));

        WebElement celdaEspecifica = tabla.findElement(By.xpath("//tbody/tr[2]/td[4]"));

        System.out.println("Datos de fila 1:" + fila.getText());
        System.out.println("Celda solicitada: " + celdaEspecifica.getText());

        esperar(TIEMPO_ESPERA_CORTO);

    }

    @Test
    public void consultarTodosDatosTabla() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_TABLAS);

        WebElement tabla = driver.findElement(By.xpath("//table[@class='dataTable']"));

        List contenedorEmpresas = tabla.findElements(By.xpath("//tbody/tr/td[1]"));

        for (int i = 0; i < contenedorEmpresas.size(); i++) {
            System.out.println("Empresa #" + (i + 1) + " - " + tabla.findElement(By.xpath("//tbody/tr["+(i+1)+"]/td[1]")).getText());
        }

        esperar(TIEMPO_ESPERA_MINIMO);
    }


    @After
    public void finish() {
        driver.quit();
    }
}
