package automationtesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Utilidades {


    public static void esperar(long tiempo) {
        tiempo = tiempo * 1000;
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void esperar(long tiempo, WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(tiempo, SECONDS);
    }

    /**
     * @param contenedorRadio Contenedor que tiene acceso al conjunto de elementos a inspeccionar
     * @param texto           Argumento que buscara dentro de los elementos inspeccionados para seleccionar
     * @author YFAC
     * @version 1.1.1
     * Método utilizado para seleccionar elementos de RADIO y SELECT de manera dinámica
     */
    public static void seleccionarDeUnaLista(List<WebElement> contenedorRadio, String texto) {
        for (int i = 0; i < contenedorRadio.size(); i++) {
            System.out.println("Elemento radio: " + contenedorRadio.get(i).getText());
            if (contenedorRadio.get(i).getText().equals(texto)) {
                contenedorRadio.get(i).click();
                break;
            }
        }
    }

    public static void seleccionarDeUnCheckBox(List<WebElement> contenedor, String texto, WebDriver driver) {
        for (int i = 0; i < contenedor.size(); i++) {
            if (contenedor.get(i).getText().equals(texto)) {
                driver.findElement(By.xpath("//div[@class='col-md-4 col-xs-4 col-sm-4']/div/input[@value='" + texto + "']")).click();
                break;
            }
        }
    }


    public static List<WebElement> mapeoXpath(String mapeo, WebDriver driver) {
        return driver.findElements(By.xpath(mapeo));
    }


    public static void seleccionarLenguaje(List<WebElement> contenedorRadio, String texto) {
        for (int i = 0; i < contenedorRadio.size(); i++) {
            System.out.println("Elemento radio: " + contenedorRadio.get(i).getAttribute("text"));
            if (contenedorRadio.get(i).getAttribute("text").equals(texto)) {
                contenedorRadio.get(i).click();
                break;
            }
        }
    }

    public static void scrollToObject(String locator, WebDriver driver) {
        WebElement element=driver.findElement(By.xpath(locator));
        Actions actions=new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }


}
