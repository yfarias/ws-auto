package automationtesting.registro;

import automationtesting.ExcelManager;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.*;

public class Register {
    private static WebDriver driver;
    private ExcelManager excelManager;


    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        excelManager = new ExcelManager();
    }

    @Test
    public void diligenciarRegistro() throws FilloException {
        driver.manage().window().maximize();

        excelManager.strRutaArchivo("C:\\WS Auto\\Basico\\PrimeraAutomatizacionWebDriver\\src\\test\\resources\\datadriven\\registro.xlsx");
        String strSql = "SELECT * FROM datos";
        Recordset objRecord = excelManager.leerExcel(strSql);
        System.out.println("La cantidad de registros en el archivo es: " + objRecord.getCount());

        while (objRecord.next()) {
            driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_REGISTRO);
            driver.findElement(By.xpath("//input[@placeholder='First Name']")).sendKeys(objRecord.getField("firstname"));
            driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys(objRecord.getField("lastname"));
            driver.findElement(By.xpath("//textarea[@ng-model='Adress']")).sendKeys(objRecord.getField("address"));
            seleccionarDeUnaLista(driver.findElements(By.xpath(CONTENEDOR_GENERO)),objRecord.getField("gender"));
            List<WebElement> hobbies = driver.findElements(By.xpath("//div[@class='col-md-4 col-xs-4 col-sm-4']/div/label"));
            String opciones = objRecord.getField("hobbies");

            List<String> listaopciones = new ArrayList<>();
            listaopciones.addAll(Arrays.asList(opciones.split(",")));
            for(int i=0;i<listaopciones.size();i++){
                seleccionarDeUnCheckBox(hobbies, listaopciones.get(i), driver);
            }

            scrollToObject(LOCATOR_PASSWORD,driver);
            driver.findElement(By.id(LOCATOR_LENGUAJE)).click();
            seleccionarLenguaje(driver.findElements(By.xpath(CONTENEDOR_LENGUAJE)),objRecord.getField("languages"));

            driver.findElement(By.id("submitbtn")).click();

            strSql = "UPDATE datos SET registrado='Registro Exitoso para el email:"+objRecord.getField("email")+"' WHERE email='"+objRecord.getField("email")+"'";
            excelManager.ModificarRegistrosExcel(strSql);
        }

    }


    @After
    public void finish() {
        driver.quit();
    }

}
