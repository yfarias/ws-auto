package automationtesting.alertas;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.esperar;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.openqa.selenium.support.ui.ExpectedConditions.alertIsPresent;

public class alertas {

    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void gestionarAlertaOK() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_ALERTAS);
        driver.findElement(By.xpath("//button[@class='btn btn-danger' and @onclick='alertbox()']")).click();

        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(alertIsPresent());

        Alert alerta = driver.switchTo().alert();
        assertEquals("I am an alert box!", alerta.getText());
        esperar(2);

        alerta.accept();
        assertEquals("Automation Demo Site", TITULO_PAGINA_AUTOMATION);
        esperar(2);
    }

    @Test
    public void gestionarAlertaOKCancel() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_ALERTAS);

        driver.findElement(By.xpath("//a[@href='#CancelTab']")).click();
        driver.findElement(By.xpath("//button[@class='btn btn-primary' and @onclick='confirmbox()']")).click();

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(alertIsPresent());

        Alert alerta = driver.switchTo().alert();
        assertEquals("Press a Button !", alerta.getText());
        esperar(2);

        alerta.accept();
        assertEquals("You pressed Ok", driver.findElement(By.xpath("//*[@id='demo']")).getText());


        driver.findElement(By.xpath("//button[@class='btn btn-primary' and @onclick='confirmbox()']")).click();
        alerta = driver.switchTo().alert();
        assertEquals("Press a Button !", alerta.getText());
        esperar(2);

        alerta.dismiss();
        alerta.dismiss();
        assertEquals("You Pressed Cancel", driver.findElement(By.xpath("//*[@id='demo']")).getText());

        esperar(2);
    }

    @Test
    public void gestionarAlertaConTextbox() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_ALERTAS);

        driver.findElement(By.xpath("//a[@href='#Textbox']")).click();
        driver.findElement(By.xpath("//button[@class='btn btn-info' and @onclick='promptbox()']")).click();

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(alertIsPresent());

        Alert alerta = driver.switchTo().alert();
        alerta.sendKeys(USERNAME);
        alerta.accept();
        assertEquals("Hello " + USERNAME + " How are you today", driver.findElement(By.id("demo1")).getText());

        driver.findElement(By.xpath("//button[@class='btn btn-info' and @onclick='promptbox()']")).click();
        alerta.sendKeys(USERNAME);
        alerta.dismiss();
        assertEquals(TITULO_PAGINA_AUTOMATION, driver.findElement(By.xpath("//*[@id='header']/div/div/div/div[2]/h1")).getText());
    }

    @After
    public void finish() {
        driver.quit();
    }
}
