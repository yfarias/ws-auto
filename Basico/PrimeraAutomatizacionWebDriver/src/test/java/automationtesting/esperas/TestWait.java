package automationtesting.esperas;

import automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.function.Function;


import static automationtesting.Constantes.URL_PRACTICE_AUTOMATION_TESTING;
import static java.util.concurrent.TimeUnit.SECONDS;

public class TestWait {
    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void esperaImplicita() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_AUTOMATION_TESTING);
        Utilidades.esperar(15,driver);
        driver.findElement(By.linkText("Shop")).click();
    }

    @Test
    public void esperaExplicita(){
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_AUTOMATION_TESTING);
        WebDriverWait wait=new WebDriverWait(driver,15);
        wait.until(visibilityOfElementLocated(By.xpath("//*[@id='site-logo']/a/img")));
        driver.findElement(By.xpath("//*[@id='site-logo']/a/img")).click();
    }

    @Test
    public void esperaFluida(){
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_AUTOMATION_TESTING);

        Wait wait=new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class)
                .ignoring(Exception.class);

        WebElement linkshop = (WebElement) wait.until(new Function<WebDriver,WebElement>(){
            public WebElement apply(WebDriver driver){
                return  driver.findElement(By.linkText("Shop"));
            }
        });

        linkshop.click();

    }


    @After
    public void finish() {
        driver.quit();
    }
}
