package automationtesting.autenticacion;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.*;
import static org.junit.Assert.assertEquals;


public class AutenticacionPagina {

    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void autenticacionPaginaExitosa() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_AUTOMATION_TESTING);
        esperar(TIEMPO_ESPERA_CORTO);
        driver.findElement(By.linkText(LINK_MY_ACCOUNT)).click();
        esperar(TIEMPO_ESPERA_CORTO);
        driver.findElement(By.id(USER_NAME)).click();
        driver.findElement(By.id(USER_NAME)).sendKeys(USERNAME);
        driver.findElement(By.id(CAMPO_PASSWORD)).click();
        driver.findElement(By.id(CAMPO_PASSWORD)).sendKeys(PASSWORD);
        driver.findElement(By.name(BTN_LOGIN)).click();

        assertEquals(TEXTO_VALIDACION, driver.findElement(By.xpath(LABEL_BIENVENIDA)).getText());
        esperar(TIEMPO_ESPERA_NORMAL);
    }

    @Test
    public void autenticacionPaginaFallida() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_AUTOMATION_TESTING);
        esperar(TIEMPO_ESPERA_CORTO);
        driver.findElement(By.linkText(LINK_MY_ACCOUNT)).click();
        esperar(TIEMPO_ESPERA_CORTO);
        driver.findElement(By.id(USER_NAME)).click();
        driver.findElement(By.id(USER_NAME)).sendKeys(USERNAME);
        driver.findElement(By.id(CAMPO_PASSWORD)).click();
        driver.findElement(By.id(CAMPO_PASSWORD)).sendKeys(PASSWORD_ERROR);
        driver.findElement(By.name(BTN_LOGIN)).click();
        assertEquals(TEXTO_VALIDACION_ERROR, driver.findElement(By.xpath(LABEL_AUTENTICACION_FALLIDA)).getText());
        esperar(TIEMPO_ESPERA_NORMAL);
    }


    @After
    public void finish() {
        driver.quit();
    }
}
