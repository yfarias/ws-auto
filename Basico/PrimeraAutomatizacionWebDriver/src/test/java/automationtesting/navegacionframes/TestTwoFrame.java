package automationtesting.navegacionframes;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static automationtesting.Constantes.*;
import static automationtesting.Utilidades.esperar;
import static org.junit.Assert.assertEquals;

public class TestTwoFrame {
    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void ingresarAUnFrame() {
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_FRAME);
        driver.findElement(By.linkText("Iframe with in an Iframe")).click();

        WebElement primerframe = driver.findElement(By.xpath("//*[@id='Multiple']/iframe"));

        if(primerframe.isDisplayed()){
            esperar(TIEMPO_ESPERA_CORTO);
            System.out.println("objeto frame 1 se visualiza");
        }else{
            System.out.println("objeto frame 1 no se visualiza");
        }

        driver.switchTo().frame(primerframe);
        WebElement segundoframe = driver.findElement(By.xpath("/html/body/section/div/div/iframe"));
        driver.switchTo().frame(segundoframe);

        esperar(TIEMPO_ESPERA_CORTO);
        driver.findElement(By.xpath(TEXTBOX_IFRAME)).click();
        driver.findElement(By.xpath(TEXTBOX_IFRAME)).sendKeys("Hola Mundo segundo frame");

        esperar(TIEMPO_ESPERA_CORTO);
        driver.switchTo().defaultContent();
        assertEquals(TITULO_PAGINA_AUTOMATION, driver.findElement(By.xpath(LABEL_AUTOMATION)).getText());
    }


    @After
    public void finish() {
        driver.quit();
    }


}
