package automationtesting.navegacionframes;

import automationtesting.Constantes;
import automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static automationtesting.Constantes.*;
import static automationtesting.Constantes.URL_PRACTICE_AUTOMATION_TESTING;
import static automationtesting.Utilidades.*;
import static org.junit.Assert.assertEquals;

public class TestOneFrame {
    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void ingresarAUnFrame(){
        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_FRAME);
        driver.switchTo().frame(PRIMER_FRAME);
        esperar(TIEMPO_ESPERA_CORTO);
        driver.findElement(By.xpath(TEXTBOX_IFRAME)).click();
        driver.findElement(By.xpath(TEXTBOX_IFRAME)).sendKeys("Hola Mundo");
        esperar(TIEMPO_ESPERA_CORTO);
        driver.switchTo().defaultContent();
        assertEquals(TITULO_PAGINA_AUTOMATION,driver.findElement(By.xpath(LABEL_AUTOMATION)).getText());
    }



    @After
    public void finish() {
        driver.quit();
    }


}
