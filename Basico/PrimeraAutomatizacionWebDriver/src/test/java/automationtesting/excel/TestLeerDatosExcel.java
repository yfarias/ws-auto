package automationtesting.excel;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.io.IOException;

import static automationtesting.Constantes.URL_PRACTICE_DEMO_AUTOMATION_TESTING_LOGIN;
import static org.junit.Assert.assertEquals;

public class TestLeerDatosExcel {
    public static WebDriver driver;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void leerDatosExcel() throws IOException {
        String rutaArchivo = "src/test/resources/datadriven/datos.xlsx";
        FileInputStream archivo = new FileInputStream(rutaArchivo);
        Workbook libro = new XSSFWorkbook(archivo); //Toma el libro del archivo
        Sheet hoja = libro.getSheetAt(0);//Parte de la primera hoja de mi archivo de excel
        Row fila = hoja.getRow(1); //Parte desde la fila 2 en el archivo de excel, ya que la fila 1 son los encabezados

        driver.manage().window().maximize();
        driver.get(URL_PRACTICE_DEMO_AUTOMATION_TESTING_LOGIN);

        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("" + fila.getCell(0));
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("" + fila.getCell(1));
        driver.findElement(By.name("login")).click();

        String usuario = String.valueOf(fila.getCell(0)).split("@")[0];

        assertEquals("Hello " + usuario + " (not " + usuario + "? Sign out)", driver.findElement(By.xpath("//*[@id='page-36']/div/div[1]/div/p[1]")).getText());

        archivo.close();

    }

    @After
    public void finish() {
        driver.quit();
    }
}
