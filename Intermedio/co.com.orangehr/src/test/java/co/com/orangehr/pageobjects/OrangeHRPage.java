package co.com.orangehr.pageobjects;

import co.com.orangehr.models.LoginData;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static co.com.orangehr.utils.Constantes.URL;
import static org.hamcrest.MatcherAssert.assertThat;

@DefaultUrl(URL)
public class OrangeHRPage extends PageObject {
    @FindBy (id="txtUsername")
    WebElementFacade USUARIO;

    @FindBy (id="txtPassword")
    WebElementFacade PASSWORD;

    @FindBy (id="btnLogin")
    WebElementFacade LOGIN;

    @FindBy (xpath="//*[@id='welcome']")
    WebElementFacade LOGIN_EXITOSO;

    public void escribirCredenciales(List<LoginData> loginData) {
        USUARIO.sendKeys(loginData.get(0).getUsername());
        PASSWORD.sendKeys(loginData.get(0).getPassword());
        LOGIN.click();
    }

    public void autenticacionExitosa(String mensaje) {
        String Welcome = LOGIN_EXITOSO.getText();
        assertThat(Welcome,containsAllText(mensaje));
    }
}
