package co.com.orangehr;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/orangehr.feature",
        tags = "@login",
        snippets = SnippetType.CAMELCASE
)
public class RunnerTags {
}
