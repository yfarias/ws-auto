package co.com.orangehr.steps;

import co.com.orangehr.models.LoginData;
import co.com.orangehr.pageobjects.OrangeHRPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class OrangeHRSteps {
    OrangeHRPage orangeHRPage;

    @Step
    public void abrirLaPagina() {
        orangeHRPage.open();
    }

    @Step
    public void autenticarse(List<LoginData> loginData) {
        orangeHRPage.escribirCredenciales(loginData);
    }

    @Step
    public void verificarLaAutenticacion(String mensaje) {
        orangeHRPage.autenticacionExitosa(mensaje);
    }
}
