/**
 * @since 27/11/2017
 */
package choucair.moviles.app;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
            features = "src/test/resources/features/login_app.feature"
            ,tags = "@LoginInTheApp"
            ,snippets = SnippetType.CAMELCASE
            ,glue = "choucair.moviles.app.definitions")

@RunWith(CucumberWithSerenity.class)
public class RunnerTags {
}