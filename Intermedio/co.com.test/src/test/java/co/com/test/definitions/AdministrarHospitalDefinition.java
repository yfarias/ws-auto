package co.com.test.definitions;

import co.com.test.models.CrearUsuarioData;
import co.com.test.steps.AdministrarHospitalSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class AdministrarHospitalDefinition {
    @Steps
    AdministrarHospitalSteps administrarHospitalSteps;

    @Given("^que el Usuario quiere registrar un Doctor$")
    public void queElUsuarioQuiereRegistrarUnDoctor() {
        administrarHospitalSteps.abriPagina();
    }

    @When("^el ingresa los datos solicitados$")
    public void elIngresaLosDatosSolicitados(List<CrearUsuarioData> crearUsuarioData) {
        administrarHospitalSteps.agregarUsuario(crearUsuarioData);
    }

    @Then("^el verificar el mensaje (.*)\\.$")
    public void elVerificarElMensajeDatosGuardadosCorrectamente(String msg) {
        administrarHospitalSteps.verificacionMensaje(msg);
    }

}
