package co.com.test.pageobject;

import co.com.test.models.CrearUsuarioData;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static co.com.test.utilidades.Constantes.URL_HOSPITAL;

@DefaultUrl(URL_HOSPITAL)
public class AdministrarHospitalPage extends PageObject {
    //******Clic Boton Adicionar Doctor
    @FindBy(xpath = "//a[@href='addDoctor']")
    WebElementFacade opAddDoctor;
    //*******Objetos para Crear Doctor
    @FindBy(id = "name")
    WebElementFacade txtNombres;
    @FindBy(id = "last_name")
    WebElementFacade txtApellidos;
    @FindBy(id = "telephone")
    WebElementFacade txtTelefono;
    @FindBy(id = "identification_type")
    WebElementFacade lstTipoDoc;
    @FindBy(id = "identification")
    WebElementFacade txtNumeroDoc;
    @FindBy(xpath = "//a[@onclick='submitForm()']")
    WebElementFacade btnGuardar;
    //***********Label Mensaje Exitoso
    @FindBy(xpath = "//div[@id='page-wrapper']//p")
    WebElementFacade lblMensaje;


    public void registrarDatosUsuario(List<CrearUsuarioData> crearUsuarioData) {
        opAddDoctor.click();
        txtNombres.sendKeys(crearUsuarioData.get(0).getNombres());
        txtApellidos.sendKeys(crearUsuarioData.get(0).getApellidos());
        txtTelefono.sendKeys(String.valueOf(crearUsuarioData.get(0).getTelefono()));
        lstTipoDoc.select().byVisibleText(crearUsuarioData.get(0).getTipoDoc());
        txtNumeroDoc.sendKeys(String.valueOf(crearUsuarioData.get(0).getNrodoc()));
        btnGuardar.click();
    }

    public void usuarioCreadoExitoso(String msg) {
        MatcherAssert.assertThat(msg,containsText(lblMensaje.getText()));
    }
}
