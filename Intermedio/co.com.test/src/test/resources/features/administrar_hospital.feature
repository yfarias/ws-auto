@Hospital
Feature: Administracion de un Hospital Publico
  Como usuario
  Quiero Administrar un hospital
  A traves de la APP Sistema de Administración de Hospitales

  @RegistrarDoctor
  Scenario Outline: Registro de un Nuevo Doctor
    Given que el Usuario quiere registrar un Doctor
    When el ingresa los datos solicitados
      |nombres          |apellidos       |telefono          |tipoDoc            |nrodoc        |
      |<nombres>      |<apellidos>       |<telefono>        |<tipoDoc>          |<nrodoc>        |
    Then el verificar el mensaje Datos guardados correctamente.
    Examples:
    |nombres          |apellidos       |telefono          |tipoDoc                    |nrodoc        |
##@externaldata@./src/test/resources/Datadriven/dtDatos.xlsx@Hoja1
   |Carlos   |Arias   |3013699878   |Cédula de ciudadanía   |21555780|
   |Pregunta   |Arias   |3013699878   |Cédula de extrangería   |41555780|

