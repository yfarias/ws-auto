package co.com.mobile.definitions;

import co.com.mobile.steps.LoginEribankSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.MatcherAssert;

import static org.hamcrest.MatcherAssert.*;

public class LoginEribankDefinition {
    @Steps
    LoginEribankSteps loginEribankSteps;

    @Given("^que Usuario quiere acceder a la App Eribak$")
    public void queUsuarioQuiereAccederALaAppEribak() {
        loginEribankSteps.openApp();
    }


    @When("^el ingresar el usuario (.*) y la clave (.*)$")
    public void elIngresarElUsuarioCompanyYLaClaveCompany(String user, String password) {
        loginEribankSteps.loginEnLaApp(user,password);
    }

    @Then("^el verifica en la aplicación el mensaje (.*)$")
    public void elVerificaEnLaAplicación(String mensaje) {
        assertThat(mensaje,loginEribankSteps.verificarAcceso(mensaje));
    }
}
