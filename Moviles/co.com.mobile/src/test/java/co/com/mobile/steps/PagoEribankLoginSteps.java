package co.com.mobile.steps;

import co.com.mobile.models.LoginData;
import co.com.mobile.pageobjects.PagoEribankLoginPage;
import io.appium.java_client.android.AndroidDriver;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class PagoEribankLoginSteps {
    PagoEribankLoginPage pagoEribankLoginPage;

    @Step
    public void logueoEnLaApp(List<LoginData> loginData) {
        pagoEribankLoginPage.diligenciarCredenciales(loginData);
    }
}
