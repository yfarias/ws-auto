package co.com.mobile.definitions;

import co.com.mobile.steps.LoginCHAcademyWebMobileSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginCHAcademyWebMobileDefinition {
    @Steps
    LoginCHAcademyWebMobileSteps loginCHAcademyWebMobileSteps;

    @Given("^que Usuario quiere Acceder a Choucair Academy$")
    public void queUsuarioQuiereAccederAChoucairAcademy() {
        loginCHAcademyWebMobileSteps.abrirLaPagina();
    }


    @When("^el ingresa las credenciales$")
    public void elIngresaLasCredenciales() {
        loginCHAcademyWebMobileSteps.realizarLogueo();
    }

    @Then("^el verifica el mensaje$")
    public void elVerificaElMensaje() {

    }
}
