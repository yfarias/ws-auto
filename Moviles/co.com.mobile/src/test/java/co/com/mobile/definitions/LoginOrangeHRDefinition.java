package co.com.mobile.definitions;

import co.com.mobile.models.LoginData;
import co.com.mobile.steps.OrangeHRSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.awt.*;
import java.util.List;

public class LoginOrangeHRDefinition {
    @Steps
    OrangeHRSteps orangeHRSteps;

    @Given("^que el Usuario quiere entrar a la APP OrangeHR$")
    public void queElUsuarioQuiereEntrarALaAPPOrangeHR() {
        orangeHRSteps.abrirLaPagina();
    }


    @When("^el escribe las credenciales$")
    public void elEscribeLasCredenciales(List<LoginData> loginData) throws InterruptedException, AWTException {
        orangeHRSteps.autenticarse(loginData);
    }

    @Then("^el verifica el mensaje de Bienvenida (.*)$")
    public void elVerificaElMensajeDeBienvenidaWelcomePaul(String mensaje) {
        orangeHRSteps.verificarLaAutenticacion(mensaje);
    }
}