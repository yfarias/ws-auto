package co.com.mobile;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
        features = "src/test/resources/features/login_eribank.feature"
        ,tags = {"@LoginExitoso"}
        ,snippets = SnippetType.CAMELCASE
        ,glue = "co.com.mobile.definitions")

@RunWith(CucumberWithSerenity.class)
public class RunnerTags {
}
