package co.com.mobile.pageobjects;

import co.com.mobile.models.PagoData;
import co.com.mobile.utils.MobilePageObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DiligenciarDatosPagoPage extends MobilePageObject {
    public DiligenciarDatosPagoPage(WebDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id="com.experitest.ExperiBank:id/makePaymentButton")
    WebElement MAKE_PAYMENT;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/phoneTextField")
    WebElement PHONE;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/nameTextField")
    WebElement NAME;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/amount")
    WebElement AMOUNT;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/countryTextField")
    WebElement COUNTRY;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/sendPaymentButton")
    WebElement SEND_PAYMENT_BUTTON;

    @AndroidFindBy(id="android:id/button1")
    WebElement SEND_PAYMENT_YES;

    public void realizarPago(List<PagoData> pagoData) {
        MAKE_PAYMENT.click();
        PHONE.sendKeys(pagoData.get(0).getPhone());
        NAME.sendKeys(pagoData.get(0).getName());
        COUNTRY.sendKeys(pagoData.get(0).getCountry());
        SEND_PAYMENT_BUTTON.click();
        AMOUNT.click();
    }
}
