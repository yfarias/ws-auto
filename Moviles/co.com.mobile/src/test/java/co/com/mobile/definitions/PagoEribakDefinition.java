package co.com.mobile.definitions;

import co.com.mobile.models.LoginData;
import co.com.mobile.models.PagoData;
import co.com.mobile.steps.DiligenciarDatosPagoSteps;
import co.com.mobile.steps.PagoEribankLoginSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class PagoEribakDefinition {
    @Steps
    PagoEribankLoginSteps pagoEribankLoginSteps;
    @Steps
    DiligenciarDatosPagoSteps diligenciarDatosPagoSteps;

    @Given("^que el usuario desea realizar un pago$")
    public void queElUsuarioDeseaRealizarUnPago(List<LoginData> loginData) {
        pagoEribankLoginSteps.logueoEnLaApp(loginData);
    }


    @When("^el diligencia los datos de la transacción$")
    public void elDiligenciaLosDatosDeLaTransacción(List<PagoData> pagoData) {
        diligenciarDatosPagoSteps.diligenciarDatos(pagoData);

    }

    @Then("^el verifica el saldo de su cuenta$")
    public void elVerificaElSaldoDeSuCuenta() {

    }
}
