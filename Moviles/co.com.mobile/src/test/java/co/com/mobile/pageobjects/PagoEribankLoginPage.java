package co.com.mobile.pageobjects;

import co.com.mobile.models.LoginData;
import co.com.mobile.utils.MobilePageObject;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PagoEribankLoginPage extends MobilePageObject {
    public PagoEribankLoginPage(WebDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id="com.experitest.ExperiBank:id/usernameTextField")
    WebElement NOMBRE_USUARIO;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/passwordTextField")
    WebElement PASSWORD;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/loginButton")
    WebElement LOGIN_BUTTON;


    public void diligenciarCredenciales(List<LoginData> loginData) {
    }
}
