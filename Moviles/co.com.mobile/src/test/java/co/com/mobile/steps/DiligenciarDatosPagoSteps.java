package co.com.mobile.steps;

import co.com.mobile.models.PagoData;
import co.com.mobile.pageobjects.DiligenciarDatosPagoPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class DiligenciarDatosPagoSteps {
    DiligenciarDatosPagoPage diligenciarDatosPagoPage;

    @Step
    public void diligenciarDatos(List<PagoData> pagoData) {
        diligenciarDatosPagoPage.realizarPago(pagoData);
    }
}
