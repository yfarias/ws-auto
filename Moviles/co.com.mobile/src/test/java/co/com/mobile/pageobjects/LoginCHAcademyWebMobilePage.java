package co.com.mobile.pageobjects;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://operacion.choucairtesting.com/academy/login/index.php")
public class LoginCHAcademyWebMobilePage extends PageObject {
    @AndroidFindBy(xpath = "//*[@id='nav-menu']/ul[2]/li/a/strong")
    WebElementFacade BUTTON_INGRESAR;


    public void diligenciarCredenciales() {
        //WebElement button = Serenity.getWebdriverManager().getCurrentDriver().findElement(By.xpath("/html/body/div[1]/div[2]/div/section/div[2]/header/div[1]/nav/ul[2]/li/a/strong"));
        //button.click();
        BUTTON_INGRESAR.click();
    }
}
