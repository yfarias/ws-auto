package co.com.mobile.pageobjects;


import co.com.mobile.models.LoginData;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.util.List;

import static co.com.mobile.utils.Utilidades.enterRobot;
import static org.hamcrest.MatcherAssert.assertThat;

@DefaultUrl("https://opensource-demo.orangehrmlive.com/index.php/auth/login")
public class OrangeHRPage extends PageObject {
    @FindBy (id="txtUsername")
    WebElementFacade USUARIO;

    @FindBy (id="txtPassword")
    WebElementFacade PASSWORD;

    @FindBy (xpath = "//input[@name='Submit']")
    WebElementFacade LOGIN;

    @FindBy (xpath="//*[@id='welcome']")
    WebElementFacade LOGIN_EXITOSO;

    public void escribirCredenciales(List<LoginData> loginData) throws InterruptedException, AWTException {
        USUARIO.sendKeys(loginData.get(0).getUsuario());
        PASSWORD.sendKeys(loginData.get(0).getPassword());
        Thread.sleep(2000);
        enterRobot();
    }

    public void autenticacionExitosa(String mensaje) {
        String Welcome = LOGIN_EXITOSO.getText();
        assertThat(Welcome,containsAllText(mensaje));
    }
}

