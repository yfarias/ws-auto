package co.com.mobile.pageobjects;

import co.com.mobile.utils.MobilePageObject;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginEribankPage extends MobilePageObject {
    public LoginEribankPage(WebDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id="com.experitest.ExperiBank:id/usernameTextField")
    WebElement NOMBRE_USUARIO;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/passwordTextField")
    WebElement PASSWORD;

    @AndroidFindBy(id="com.experitest.ExperiBank:id/loginButton")
    WebElement LOGIN_BUTTON;

    @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.webkit.WebView/android.webkit.WebView/android.view.View")
    WebElement MENSAJE;



    public void diligenciarCredenciales(String user, String password) {
        NOMBRE_USUARIO.sendKeys(user);
        PASSWORD.sendKeys(password);
        LOGIN_BUTTON.click();
    }

    public boolean verificarAccesoExitoso(String mensaje) {
        String mensaje_pantalla= MENSAJE.getText();
        return mensaje.equals(mensaje_pantalla);
    }
}
