package co.com.mobile.steps;

import co.com.mobile.pageobjects.LoginEribankPage;
import net.thucydides.core.annotations.Step;

public class LoginEribankSteps {
    LoginEribankPage loginEribankPage;

    @Step
    public void openApp() {
    }

    @Step
    public void loginEnLaApp(String user, String password) {
        loginEribankPage.diligenciarCredenciales(user,password);
    }

    public boolean verificarAcceso(String mensaje) {
        return loginEribankPage.verificarAccesoExitoso(mensaje);
    }
}
