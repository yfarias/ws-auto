package co.com.mobile.utils;

import com.sun.javafx.scene.traversal.Direction;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.Rectangle;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.time.Duration;

public class Utilidades {
//    /**
//     * Performs swipe inside an element
//     *
//     * @param el  the element to swipe
//     * @param dir the direction of swipe
//     * @version java-client: 7.3.0
//     **/
//    public void swipeElementAndroid(MobileElement el, Direction dir) {
//        System.out.println("swipeElementAndroid(): dir: '" + dir + "'"); // always log your actions
//
//        // Animation default time:
//        //  - Android: 300 ms
//        //  - iOS: 200 ms
//        // final value depends on your app and could be greater
//        final int ANIMATION_TIME = 200; // ms
//
//        final int PRESS_TIME = 200; // ms
//
//        int edgeBorder;
//        PointOption pointOptionStart, pointOptionEnd;
//
//        // init screen variables
//        Rectangle rect = el.getRect();
//        // sometimes it is needed to configure edgeBorders
//        // you can also improve borders to have vertical/horizontal
//        // or left/right/up/down border variables
//        edgeBorder = 0;
//
//        switch (dir) {
//            case DOWN: // from up to down
//                pointOptionStart = PointOption.point(rect.x + rect.width / 2,
//                        rect.y + edgeBorder);
//                pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
//                        rect.y + rect.height - edgeBorder);
//                break;
//            case UP: // from down to up
//                pointOptionStart = PointOption.point(rect.x + rect.width / 2,
//                        rect.y + rect.height - edgeBorder);
//                pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
//                        rect.y + edgeBorder);
//                break;
//            case LEFT: // from right to left
//                pointOptionStart = PointOption.point(rect.x + rect.width - edgeBorder,
//                        rect.y + rect.height / 2);
//                pointOptionEnd = PointOption.point(rect.x + edgeBorder,
//                        rect.y + rect.height / 2);
//                break;
//            case RIGHT: // from left to right
//                pointOptionStart = PointOption.point(rect.x + edgeBorder,
//                        rect.y + rect.height / 2);
//                pointOptionEnd = PointOption.point(rect.x + rect.width - edgeBorder,
//                        rect.y + rect.height / 2);
//                break;
//            default:
//                throw new IllegalArgumentException("swipeElementAndroid(): dir: '" + dir + "' NOT supported");
//        }
//
//        // execute swipe using TouchAction
//        try {
//            new TouchAction(Serenity.getWebdriverManager().getCurrentDriver())
//                    .press(pointOptionStart)
//                    // a bit more reliable when we add small wait
//                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
//                    .moveTo(pointOptionEnd)
//                    .release().perform();
//        } catch (Exception e) {
//            System.err.println("swipeElementAndroid(): TouchAction FAILED\n" + e.getMessage());
//            return;
//        }
//
//        // always allow swipe action to complete
//        try {
//            Thread.sleep(ANIMATION_TIME);
//        } catch (InterruptedException e) {
//            // ignore
//        }
//    }

    public static void enterRobot() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_ENTER);

    }


}
