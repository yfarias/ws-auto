package co.com.mobile.steps;

import co.com.mobile.pageobjects.LoginCHAcademyWebMobilePage;
import net.thucydides.core.annotations.Step;

public class LoginCHAcademyWebMobileSteps {
    LoginCHAcademyWebMobilePage loginCHAcademyWebMobilePage;

    @Step
    public void abrirLaPagina() {
        loginCHAcademyWebMobilePage.open();
    }

    @Step
    public void realizarLogueo() {
        loginCHAcademyWebMobilePage.diligenciarCredenciales();
    }
}
