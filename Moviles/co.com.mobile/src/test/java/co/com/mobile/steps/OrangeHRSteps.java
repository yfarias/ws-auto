package co.com.mobile.steps;

import co.com.mobile.models.LoginData;
import co.com.mobile.pageobjects.OrangeHRPage;
import net.thucydides.core.annotations.Step;

import java.awt.*;
import java.util.List;

public class OrangeHRSteps {
    OrangeHRPage orangeHRPage;

    @Step
    public void abrirLaPagina() {
        orangeHRPage.open();
    }

    @Step
    public void autenticarse(List<LoginData> loginData) throws InterruptedException, AWTException {
        orangeHRPage.escribirCredenciales(loginData);
    }

    @Step
    public void verificarLaAutenticacion(String mensaje) {
        orangeHRPage.autenticacionExitosa(mensaje);
    }
}

