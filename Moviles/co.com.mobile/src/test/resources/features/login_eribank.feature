@Test
Feature: Operaciones en la App Eribank
  Como usuario
  Quiero Acceder
  A la App de Eribank

  @LoginExitoso
  Scenario: Logueo Exitoso en la App de Eribank
    Given que Usuario quiere acceder a la App Eribak
    When el ingresar el usuario company y la clave company
    Then el verifica en la aplicación el mensaje Your balance is: 100.00$

  @RealizarPago
  Scenario: Realizar un pago a traves de la APP
    Given que el usuario desea realizar un pago
    |usuario  |password |
    |company  |company  |
    When el diligencia los datos de la transacción
    |phone      |name         |country  |
    |3013566666 |Yeison Arias |Colombia |
    Then el verifica el saldo de su cuenta
